import 'package:flutter/material.dart';
import 'package:tugas_kampus/views/home_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter MVC Demo',
      theme: ThemeData(
        primarySwatch: MaterialColor(0xFF910A00, <int, Color>{
          50: Color(0xFFE58E87),
          100: Color(0xFFE58E87),
          200: Color(0xFFE58E87),
          300: Color(0xFFE58E87),
          400: Color(0xFFE58E87),
          500: Color(0xFF910A00),
          600: Color(0xFFE58E87),
          700: Color(0xFFE58E87),
          800: Color(0xFFE58E87),
          900: Color(0xFFE58E87),
        }),
      ),
      home: HomeView(),
    );
  }
}
